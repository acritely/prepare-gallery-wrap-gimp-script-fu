# Prepare Gallery Wrap
This is a script to add border panels to an image in preparation for printing as a canvas wrap. It creates a reflection of each side of the original image and returns an image with a single visible layer. The input is the width of the borders in pixels. 

![alt text](resources/preview.png "Example usage.")

To install the script, copy the file to the path where GIMP stores scripts. The file 'deploy_script.sh' has the path in the linux environment. The GIMP Script-Fu tutorial has more information: https://docs.gimp.org/2.10/en/gimp-using-script-fu-tutorial-first-script.html

This is [free software](urlhttps://www.gnu.org/licenses/gpl-3.0.en.html) enjoy!
